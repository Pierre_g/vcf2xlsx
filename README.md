# vcf2xlsx

vcf2xlsx allow to convert all VCF (with or without VEP annotation) file into xlsx files. 

## General

### Generate CONDA envrionnements

`conda env create -f makexcel.yml`

### Activate environment

`conda activate makexcel`

### Execute script

`python3 makeExcel.py -i ./data/sample.vcf -c ./conf_yml/STB/1.0/STB.yml -n SAMPLE_1 -tr ./conf_yml/STB/1.0/Liste_NM_TSC_2021.txt`

## Configuration file
 Configuration file allow to select columns with regex annotation, create empty columns with specific header and you can freeze several lines if you need with freeze_cols and freeze_rows values.

 Moreover create new coumns and apply specific filter with your configuration file. 

 Example: 
```
        REF_TRANS: 
            Yes:
                formatage:
                    - transcrit_list
                filtre: >
                    Feature.str.split('.',expand=True)[0].isin({transcrit_list})

            No:
                formatage:
                    - transcrit_list
                filtre: >
                    ~Feature.str.split('.',expand=True)[0].isin({transcrit_list})

```


Here you wil create REF_TRANS column, and it will fill by Yes if column "Feature" had a value in transcrit_list variable and vice versa.


