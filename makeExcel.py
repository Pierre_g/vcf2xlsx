
import argparse
import tempfile
import logging
import pandas as pd
import tqdm
import vcfpy
from functions import fillHeader, getVariant, reformatDf,writeExcel,getConfigDf
from joblib import Parallel, delayed, load
import os
import time
import yaml
import shutil
def vcf2Df(vcfpath, output, confPathYAML,
            thread,vep_ontology,keyColsConcat,
            blanketPath,prefix,transcritsFiles):
    """
    vcf2Df parse vcf into pandas dataframe.

    Main steps: 

    - Read vcf with vcfpy. 
    - Create two dict (template_df containing emptying columns 
      detecting in header and metadata containing 
      all information about sample, format and infos 
      columns in vcf. Iterate on variant in
      parallel to fill template. 
    - Reformat all sub dataframe according to config file 
      if it is indicate.

    Args:
        path (_type_): _description_

    Returns:
        _type_: _description_


    """

    # VARIABLES INITIALISATIONS
    dict_df = {"Variants":[]}
    subList_size = 2
    confDict = {}
    #tmpFolder = tempfile.TemporaryDirectory()
    tmpFolder = "/data/tmp/"
    transcritsVar = ["transcrit_list"]

    reader = vcfpy.Reader.from_path(vcfpath)

    templateDf, metadata = fillHeader(reader)
    
    if confPathYAML != "":
            # Application des filtres du fichier de configuration
        with open(confPathYAML) as confile:
            confDict = yaml.load(confile, Loader=yaml.FullLoader)
            
            dict_df["Config"] = getConfigDf(confDict)
            
    if blanketPath != "":
          col = ["Gene","Transcrit","Exon"]
          
          
          col.extend(metadata['sample_list'])
          df = pd.read_excel(blanketPath,
                             engine="openpyxl",sheet_name='MIN_COVERAGE')
          dict_df["Blanket"] = [df.filter(col)]

    if len(transcritsFiles) != 0:
        
        if len(transcritsFiles) != len(transcritsVar):
            raise Exception(f"Different size of list: {transcritsFiles} vs {transcritsVar}")
        else:
            
        
          for title,variable in list(zip(transcritsVar,transcritsFiles)):
              if variable != "":
                  # print(title)
                  # print(variable)
                  # print(confDict)
                  # raise
                  confDict[title] = variable


    # print(metadata)
    # raise
    results = Parallel(n_jobs=thread, verbose=1)(delayed(getVariant)
                                                 (record, templateDf,
                                                 metadata, tmpFolder) for record in reader)
    print("Reformat")

    if confPathYAML != "":
      results = Parallel(n_jobs=thread, verbose=0)(delayed(reformatDf)
                                                      (listpath=list_path, confDict=confDict,
                                                      tmp_dir=tmpFolder,
                                                      ontology_path=vep_ontology,KeyColsForConcat=keyColsConcat,sampleList = metadata["sample_list"])for list_path in 
                                                      tqdm.tqdm([results[i:i+subList_size] for i in range(0, len(results),subList_size )] ))


      #Correct results

      for dictResult in results:
            if dictResult != None:
              for catKeys in dictResult.keys():
                cat,len_cat = catKeys.split("-")

                if cat not in dict_df.keys():
                    
                    dict_df[cat] = []
                    
                dict_df[cat].append(dictResult[catKeys])
              
    else: 
      dict_df["VCF"] = []
      for path in results:
            dict_df["VCF"].append(path)
      
    #dict_df["Variants"].extend(results)

    writeExcel(dict_df,
               f"{output}/{prefix}.xlsx",
               metadata["sample_list"],confDict)
    
    # shutil.rmtree(tmpFolder)
    return 


parser = argparse.ArgumentParser(description="""makexcel can parse your VCF file into excel file.
                                 In condition that you respect our vcf format (see README.md) """,
                                 epilog=" Pierre GUERACHER - CHU Angers 2022")
parser = argparse.ArgumentParser()

parser.add_argument("-i", "--input", type=str,
                    help="""VCF file to parse. (Gzipped or not)""",
                    required=True)
parser.add_argument("-o", "--output", type=str,
                    help="""Output folder""",
                    required=False, default="./")
parser.add_argument("-p","--project",type=str,required=False,default="")
parser.add_argument("-c", "--conf", type=str, help="""YML file with your configuration. 
                    (File to import, filters, columns to rename and columns to keep)""",
                    required=False, default="")
parser.add_argument("-b", "--blanket", type=str, help="""Blanket xlsx file where all patent are present""",
                    required=False, default="")
parser.add_argument("-tr", "--transcrits", type=str, help="""All files with transcrits list in specific 
                    order [REFTRANS].""",
                    required=False, nargs="+")
parser.add_argument("-n","--name",type=str,required=True)
parser.add_argument("-v","--version",action='version',version='%(prog) 0.0')
parser.add_argument("-oy","--ontologies",type=str,help="""VEP ontology file to 
reformat Consequence columns if your vcf is annotated wih VEP.""",required=False,
default=f"{os.path.dirname(os.path.realpath(__file__))}/vep_ontology.txt")
parser.add_argument("-t", "--thread", type=int,
                    help="Thread number to use in parallel", required=False,
                    default=6)

args = parser.parse_args()

if __name__ == '__main__':
    st = time.time() 
    print(args.transcrits)
    vcf2Df(vcfpath=args.input, output=args.output,
           confPathYAML=args.conf, thread=args.thread,
           vep_ontology=args.ontologies,
           keyColsConcat={"CSQ":"FeatureWithoutVersion",
                          "SPiP":"transcript"},
           blanketPath=args.blanket,prefix=args.name
           ,transcritsFiles=args.transcrits)
    et = time.time()
    elapsed_time = et - st
    print('Execution time:', elapsed_time, 'seconds')
    # logging.warning("""Script not tested on multi-sample vcf!
    #               If you are using multi sample files. 
    #               Check output consistency""",stacklevel=0)
    
