import collections
import copy
import re
import uuid
import numpy as np
import pandas as pd
from joblib import dump, load
from dictsearch.search import iterate_dictionary
from copy import deepcopy
import xlsxwriter


def fillHeader(readerObj):
    """
    Reader header of your vcf file and create an empty template 
    of pandas dataframe record all FORMAT and INFO data in
    dictionnary.


    Args:
        readerObj (Reader): Object from vcfpy.Reader 

    Raises:
        Exception: raise Exception if INFO field doesn't have a Number
                   (see: https://samtools.github.io/hts-specs/VCFv4.2.pdf )
    Returns:
        _tuple_: Return tuple with two dict. 
         - dicoToxlsx with all columns detected in vcf header.
            Columns CHR, POS, REF,ALT,FILTER are imposed. INFOS and FORMAT fields
            are parsed according to header informations

         - dicoMetadata contains all vcf metadata, sample list, format and infos columns
            with they number, type and description and names of the parsed columns
    """

    dicoToxlsx = {"CHR": {}, "POS": {}, "REF": {}, "ALT": {}, "FILTER": {}}
    dicoMetadata = {
        "sample_list": readerObj.header.samples.names, "format": {}, "infos": {}}

    # Get Format results
    for format in readerObj.header.format_ids():
        dictColFormat = {}
        pipeFormat = False
        number = readerObj.header.get_format_field_info(format).number
        description = readerObj.header.get_format_field_info(
            format).description
        typeFormat = readerObj.header.get_format_field_info(format).type
        
                # if type == String check if pipe columns at the end of description
        if typeFormat == "String":

            pipeFormat = bool(re.findall(
                "([A-Za-z0-9_]*\|[A-Za-z0-9_]*)+", description))
            # If pipe is detected
            if pipeFormat:
                # infoColum will contain all str between |. They will be futur column on df.
                formatColumn = "".join(re.findall("([A-Za-z0-9_*]*\|[A-Za-z0-9_|]*)+",
                                                description)).strip().split("|")
            else:
                formatColumn = [format]
        else:
            formatColumn = [format]

        for sampleIndex, sample in enumerate(dicoMetadata["sample_list"]):
            
            for index_column,colFormated in enumerate(formatColumn):

                dicoToxlsx[f"{sample}\n{colFormated}"] = {}
                

                dictColFormat[f"{index_column}-{sampleIndex}"]= f"{sample}\n{colFormated}"


        dicoMetadata["format"][format] = {"number": number, "type": typeFormat,
                                          "description": description,
                                          "source": "", "columns": dictColFormat,
                                          "pipeFormat":pipeFormat}

    # Get Infos results

    for info_category in readerObj.header.info_ids():

        # Recover all information about each INFO field (i.e ID)
        dictColInfo = {}
        pipeInfo = False
        number = readerObj.header.get_info_field_info(info_category).number
        description = readerObj.header.get_info_field_info(
            info_category).description
        typeInfo = readerObj.header.get_info_field_info(info_category).type

        # if type == String check if pipe columns at the end of description
        if typeInfo == "String":

            pipeInfo = bool(re.findall(
                "([A-Za-z0-9_]*\|[A-Za-z0-9_]*)+", description))
            # If pipe is detected
            if pipeInfo:
                # infoColum will contain all str between |. They will be futur column on df.
                infoColumn = "".join(re.findall("([A-Za-z0-9_*]*\|[A-Za-z0-9_|]*)+",
                                                description)).strip().split("|")
            else:
                # Else big category will be in column name (like CSQ for VEP)
                infoColumn = [info_category]

        else:
            infoColumn = [info_category]

        # For win times all infoColumn have save treatment.
        for index_subInfo, name_subInfo in enumerate(infoColumn):

            # If number G, value depend on number of genotype
            if number == "G":
                # Iterate on each sample in VCF
                for sampleIndex, sample in enumerate(dicoMetadata["sample_list"]):

                    dicoToxlsx[f"{sample}\n{name_subInfo}"] = {}

                    # Allow to link big category to name_subInfo more
                    # later in script.
                    if f"{info_category}_{sampleIndex}" not in dictColInfo.keys():
                        dictColInfo[f"{info_category}_{sampleIndex}"] = {}
                    dictColInfo[f"{info_category}_{sampleIndex}"][index_subInfo] = f"{sample}\n{name_subInfo}"

            # If number 1 or A. We choose to concatenate on same line if several allele
            # At the same position. So A or 1 are treated similary
            
            elif isinstance(number,int) or number == "A" :

                dicoToxlsx[name_subInfo] = {}
                dictColInfo[index_subInfo] = name_subInfo

            # If number is . His more complicated, so to correct values more later
            # We choose to name column with big category.
            elif number == ".":

                dicoToxlsx[f"{info_category}___{name_subInfo}"] = {}

                # Nommage different pour correctif post import pandas
                dictColInfo[f"{info_category}_{index_subInfo}"] = f"{info_category}___{name_subInfo}"
                # listColInfo.append(column)
    
            else:
                print(type(number))
                print(description)
                print(typeInfo)
                raise Exception(
                    f"Please specify number in vcf header for {info_category}")

        dicoMetadata["infos"][info_category] = {"number": number, "type": typeInfo,
                                                "description": description, "source": "",
                                                "columns": dictColInfo, "pipeInfo": pipeInfo}

    return dicoToxlsx, dicoMetadata


def orderDict(listDict):
    dictAllDict = {}
    listFinalDict = []

    for sub_dict in listDict:

        hasKey = hash(frozenset(sub_dict.items()))

        if hasKey not in dictAllDict.keys():
            dictAllDict[hasKey] = []

        dictAllDict[hasKey].append(sub_dict)

    for key, value in dictAllDict.items():

        listFinalDict.extend(value)
    return listFinalDict

def changeHGVSp(hgvsp):
    """
    Add parenthesis on HGVSp terms 
    in order to meet the HGVSp 
    nomenclature of protein predictions
    """    
    try: 
        nm, mut = hgvsp.split(":p.")

        correctedHGVSp = nm+":p.("+mut+")"
        
    except:
        correctedHGVSp = hgvsp
        
    return correctedHGVSp


def correctDictValues(df):
    """
    _summary_

    Args:
        df (_type_): _description_

    Raises:
        Exception: _description_

    Returns:
        _type_: _description_
    """
    categoryDict = {}
    dictDf       = {}
    dictRowsLine = {}
    dicoToDfCorrected = {}
    vepAnnotated = False
    listColNameNoDict = list(filter(lambda x: "___" not in x, df.columns))
    listColNameDict = list(filter(lambda x: "___" in x, df.columns))

    if len(listColNameDict) > 0:
        infosInVCF = True

        for category in listColNameDict:
            titleCat = category.split("___")[0]

            if titleCat == "CSQ":
                vepAnnotated = True

            if titleCat not in categoryDict.keys():
                categoryDict[titleCat] = category
    else:
        return df

    for index, df_line in df.iterrows():
        dictCatPointNumbLen = {}
        for cat in categoryDict.keys():
            value = df_line[categoryDict[cat]]

            if isinstance(value, dict):
                lenDictValue = len(value.keys())

            else:
                lenDictValue = 1

            if lenDictValue not in dictCatPointNumbLen.keys():

                dictCatPointNumbLen[lenDictValue] = []
                    
            dictCatPointNumbLen[lenDictValue].append(cat)

        # Pour chaque ligne du tableau de(s) variant.ALT

        # Recupere une "ligne de base" correspondant aux
        # colonnes n'ayant de dictionnaire en valeur
        dictCatPointNumbLenSorted = collections.OrderedDict(sorted(dictCatPointNumbLen.items(),reverse=True)) 
        listTuple = []
        for lenCat,listCat in dictCatPointNumbLenSorted.items():
        
            for titlecat in listCat:
                
                listTuple.append((lenCat,titlecat))
        
        for lenAnnot,annotKey  in listTuple:

            if vepAnnotated:
                if annotKey == "CSQ":

                    baseLine = df_line[listColNameNoDict].to_dict()
                else:
                    baseLine = df_line[["CHR", "POS", "REF", "ALT"]].to_dict()
            else:
                baseLine = df_line[listColNameNoDict].to_dict()
            # Recupere les colonnes de la categorie cible
            colList = []

            colList.extend(
                list(filter(lambda x: f"{annotKey}___" in x, listColNameDict)))
            # A chaque catégorie: copie la ligne de base autant
            # de fois que la longueur du dictionnaire en valeur

            # Dans le cas où plusieurs cat dans dictCa11tPointNumbLenSorted.
            # le type de baseLine change selon les iterations.
            # Afin de conserver la même méthode selon les catégories
            # Necessiare de vérifier type de la variable
            # if lenAnnot == None:

            #     lenAnnot = 1

            if isinstance(baseLine, list):
                baseLine_loop_brut = baseLine * lenAnnot

            else:
                baseLine_loop_brut = [baseLine] * lenAnnot

            try: baseLine_loop = orderDict(baseLine_loop_brut)
            except: baseLine_loop = copy.deepcopy(baseLine_loop_brut)

            count_index = -1
            iterator = -1
            baseLine_Final = []
            while iterator < len(baseLine_loop)-1:
                count_index += 1
                iterator += 1

                # When you multiply dict in list they shared same memory
                lineToModif = deepcopy(baseLine_loop[iterator])

                for columnDictValue in colList:
                    try:
                        valueDict = df_line[columnDictValue][count_index]
                    except KeyError:
                        count_index = 0
                        valueDict = df_line[columnDictValue][count_index]
                        
                    except TypeError:
                        continue

                    colRenammed = columnDictValue.split("___")[1]

                    if colRenammed in baseLine_loop[iterator].keys():
                        print(f"Duplicate columns detected {columnDictValue}")

                        colRenammed = columnDictValue

                    lineToModif[colRenammed] = valueDict

                baseLine_Final.append(lineToModif)

            # baseLine = deepcopy(baseLine_Final)

            if index not in dictRowsLine.keys():

                dictRowsLine[index] = {}

            dictRowsLine[index][annotKey] = baseLine_Final

    for old_row_index in dictRowsLine.keys():

        for annotKey in dictRowsLine[old_row_index].keys():

            if annotKey not in dicoToDfCorrected.keys():
                dicoToDfCorrected[annotKey] = {}

            for dict_index, dict_row in enumerate(dictRowsLine[old_row_index][annotKey]):

                dicoToDfCorrected[annotKey][f"{dict_index}-{old_row_index}"] = dict_row

    for titlecategory in dicoToDfCorrected.keys():

        dictDf[titlecategory] = pd.DataFrame(
            dicoToDfCorrected[titlecategory]).T

    return dictDf


def getVariant(recordObjFct, dicoDf, dicoMet, tmp_dir):
    """_summary_

    Args:
        recordObjFct (_type_): _description_
        dicoDf (_type_): _description_
        dicoMet (_type_): _description_
        tmp_dir (_type_): _description_

    Raises:
        Exception: _description_

    Returns:
        _type_: _description_
    """

    dicoToxlsx = copy.deepcopy(dicoDf)
    recordObj = copy.deepcopy(recordObjFct)

    dicoMetadata = copy.deepcopy(dicoMet)

    iterator = uuid.uuid4().hex

    dicoToxlsx["CHR"][iterator] = recordObj.CHROM
    dicoToxlsx["POS"][iterator] = recordObj.POS
    dicoToxlsx["REF"][iterator] = recordObj.REF
    dicoToxlsx["ALT"][iterator] = ",".join(
        [alt.value for alt in recordObj.ALT])

    dicoToxlsx["FILTER"][iterator] = ",".join(recordObj.FILTER)

    # Get format value for each sample
    
    if len(recordObj.calls) != 0:
        for index_call, value_call in enumerate(recordObj.calls):

            for formatKey in dicoMetadata["format"].keys():
                callForVal = value_call.data.get(formatKey)
                
                if isinstance(callForVal,list) == False:
                    values = [callForVal]
                
                elif isinstance(callForVal,list):
                
                    if len(value_call.data.get(formatKey)) == 1:
                        
                    
                        if dicoMetadata["format"][formatKey]["pipeFormat"]:

                            values = value_call.data.get(formatKey)[0].split("|")
                            
                        else:
                            values = value_call.data.get(formatKey)
                            
                    else:
                        if dicoMetadata["format"][formatKey]["pipeFormat"]:
                            subList = []
                            values = []
                            for val in value_call.data.get(formatKey):
                                
                                subList.append(val.split("|"))
                            
                            for concatValues in list(zip(subList)):
                                
                                values.append(",".join(concatValues))     
                        else:
                            
                            values =[",".join(
                                map(str,value_call.data.get(formatKey)))]
                        
                for indexColVal, colVal in enumerate(values):
                    
                    colName = dicoMetadata["format"][formatKey]["columns"][f"{indexColVal}-{index_call}"]
                    dicoToxlsx[colName][iterator] = colVal


            for info_category in recordObj.INFO.keys():

                if info_category not in dicoMetadata["infos"].keys():
                    raise Exception(f"""
                                    
                    {info_category} not find in your vcf header. 
                    Please fill your according to vcf specification
                    """)

                info_value = recordObj.INFO[info_category]

                if dicoMetadata["infos"][info_category]["number"] == 1:
                    if dicoMetadata["infos"][info_category]["pipeInfo"]:
                        list_info_1 = info_value.split('|')

                        for indexInfo1, valueInfo1 in enumerate(list_info_1):
                            colName_1 = dicoMetadata["infos"][info_category]["columns"][indexInfo1]
                            dicoToxlsx[colName][iterator] = valueInfo1
                    else:
                        colName_1 = dicoMetadata["infos"][info_category]["columns"][0]

                        dicoToxlsx[colName_1][iterator] = info_value
                        
                elif dicoMetadata["infos"][info_category]["number"] == 2:
                        colName_1 = dicoMetadata["infos"][info_category]["columns"][0]

                        dicoToxlsx[colName_1][iterator] = info_value
                        
                elif dicoMetadata["infos"][info_category]["number"] == 0:
                    colName_1 = dicoMetadata["infos"][info_category]["columns"][0]

                    dicoToxlsx[colName_1][iterator] = colName_1
                    

                elif dicoMetadata["infos"][info_category]["number"] == "A":
                    if dicoMetadata["infos"][info_category]["pipeInfo"]:
                        intermediate_list = []
                        infoJoined = []
                        intermediate_list.extend(
                            [info_A.split("|") for info_A in info_value])

                        zippedList = zip(*intermediate_list)

                        infoJoined.extend([",".join(interm_var)
                                        for interm_var in zippedList])

                        for indexInfoA, valueInfoA in enumerate(infoJoined):

                            colName = dicoMetadata["infos"][info_category]["columns"][indexInfoA]

                            dicoToxlsx[colName][iterator] = valueInfoA

                    else:
                        colName_A = dicoMetadata["infos"][info_category]["columns"][0]

                        try:
                            dicoToxlsx[colName_A][iterator] = ",".join(info_value)
                        except TypeError:
                            listValue = [str(val) for val in info_value]

                            dicoToxlsx[colName_A][iterator] = ",".join(listValue)

                elif dicoMetadata["infos"][info_category]["number"] == "G":
                    if dicoMetadata["infos"][info_category]["pipeInfo"]:
                        splittedGInfo = info_value[index_call].split("|")
                        for indexGInfo, valueGInfo in enumerate(splittedGInfo):

                            colName_G = dicoMetadata["infos"][info_category][
                                "columns"][f"{info_category}_{index_call}"][indexGInfo]
                            dicoToxlsx[colName_G][iterator] = valueGInfo
                    else:
                        colName_G = dicoMetadata["infos"][info_category][
                            "columns"][f"{info_category}_{index_call}"][0]
                        dicoToxlsx[colName_G][iterator] = info_value[index_call]

                elif dicoMetadata["infos"][info_category]["number"] == ".":
                    if dicoMetadata["infos"][info_category]["pipeInfo"]:
                        for index_listAnnotationPoint, value_listAnnotPoint in enumerate(info_value):
                            splitted_values = value_listAnnotPoint.split("|")

                            for index_subInfoPoint, value_subInfoPoint in enumerate(splitted_values):
                                colNamePoint = dicoMetadata["infos"][info_category][
                                    "columns"][f"{info_category}_{index_subInfoPoint}"]

                                if iterator not in dicoToxlsx[colNamePoint].keys():
                                    dicoToxlsx[colNamePoint][iterator] = {}

                                dicoToxlsx[colNamePoint][iterator][index_listAnnotationPoint] = value_subInfoPoint
                    else:

                        for index_listAnnotation, value_listAnnot in enumerate(info_value):
                            
                            if isinstance(value_listAnnot,list):
                                for index_subInfoPoint, value_subInfoPoint in enumerate(value_listAnnot):

                                    try: colName = dicoMetadata["infos"][info_category][
                                        "columns"][f"{info_category}_{index_subInfoPoint}"]
                                    
                                    except:
                                        print(info_value)

                                        print(dicoMetadata["infos"])
                                        raise

                                    if iterator not in dicoToxlsx[colName].keys():
                                        dicoToxlsx[colName][iterator] = {}

                                    dicoToxlsx[colName][iterator][index_listAnnotation] = value_subInfoPoint
                            else:
                                colName = dicoMetadata["infos"][info_category][
                                        "columns"][f"{info_category}_0"]
                                if iterator not in dicoToxlsx[colName].keys():
                                            dicoToxlsx[colName][iterator] = {}
                                dicoToxlsx[colName][iterator][index_listAnnotation] = value_listAnnot
                                
                                
    else:
        for info_category in recordObj.INFO.keys():
    
            if info_category not in dicoMetadata["infos"].keys():
                raise Exception(f"""
                                
                {info_category} not find in your vcf header. 
                Please fill your according to vcf specification
                """)

            info_value = recordObj.INFO[info_category]

            if dicoMetadata["infos"][info_category]["number"] == 1:
                if dicoMetadata["infos"][info_category]["pipeInfo"]:
                    list_info_1 = info_value.split('|')

                    for indexInfo1, valueInfo1 in enumerate(list_info_1):
                        colName_1 = dicoMetadata["infos"][info_category]["columns"][indexInfo1]
                        dicoToxlsx[colName][iterator] = valueInfo1
                else:
                    colName_1 = dicoMetadata["infos"][info_category]["columns"][0]

                dicoToxlsx[colName_1][iterator] = info_value
                    
            elif dicoMetadata["infos"][info_category]["number"] == 2:
                    colName_1 = dicoMetadata["infos"][info_category]["columns"][0]

                    dicoToxlsx[colName_1][iterator] = info_value
                    
            elif dicoMetadata["infos"][info_category]["number"] == 0:
                colName_1 = dicoMetadata["infos"][info_category]["columns"][0]

                dicoToxlsx[colName_1][iterator] = colName_1
                

            elif dicoMetadata["infos"][info_category]["number"] == "A":
                if dicoMetadata["infos"][info_category]["pipeInfo"]:
                    intermediate_list = []
                    infoJoined = []
                    intermediate_list.extend(
                        [info_A.split("|") for info_A in info_value])

                    zippedList = zip(*intermediate_list)

                    infoJoined.extend([",".join(interm_var)
                                    for interm_var in zippedList])

                    for indexInfoA, valueInfoA in enumerate(infoJoined):

                        colName = dicoMetadata["infos"][info_category]["columns"][indexInfoA]

                        dicoToxlsx[colName][iterator] = valueInfoA

                else:
                    colName_A = dicoMetadata["infos"][info_category]["columns"][0]

                    try:
                        dicoToxlsx[colName_A][iterator] = ",".join(info_value)
                    except TypeError:
                        listValue = [str(val) for val in info_value]

                        dicoToxlsx[colName_A][iterator] = ",".join(listValue)

            elif dicoMetadata["infos"][info_category]["number"] == ".":
                if dicoMetadata["infos"][info_category]["pipeInfo"]:
                    for index_listAnnotationPoint, value_listAnnotPoint in enumerate(info_value):
                        splitted_values = value_listAnnotPoint.split("|")

                        for index_subInfoPoint, value_subInfoPoint in enumerate(splitted_values):
                            colNamePoint = dicoMetadata["infos"][info_category][
                                "columns"][f"{info_category}_{index_subInfoPoint}"]

                            if iterator not in dicoToxlsx[colNamePoint].keys():
                                dicoToxlsx[colNamePoint][iterator] = {}

                            dicoToxlsx[colNamePoint][iterator][index_listAnnotationPoint] = value_subInfoPoint
                else:
                    try:
                        for index_listAnnotation, value_listAnnot in enumerate(info_value):
                            
                            if isinstance(value_listAnnot,list):
                                for index_subInfoPoint, value_subInfoPoint in enumerate(value_listAnnot):

                                    colName = dicoMetadata["infos"][info_category][
                                        "columns"][f"{info_category}_{index_subInfoPoint}"]

                                    if iterator not in dicoToxlsx[colName].keys():
                                        dicoToxlsx[colName][iterator] = {}

                                    dicoToxlsx[colName][iterator][index_listAnnotation] = value_subInfoPoint
                            else:
                                colName = dicoMetadata["infos"][info_category][
                                        "columns"][f"{info_category}_0"]
                                if iterator not in dicoToxlsx[colName].keys():
                                            dicoToxlsx[colName][iterator] = {}
                                dicoToxlsx[colName][iterator][index_listAnnotation] = value_listAnnot
                                
                    except:
                        print(info_category)
                        print(dicoMetadata["infos"][info_category])
                        print(info_value)
                        raise
            

    dump_name = uuid.uuid4().hex
    path_dump = tmp_dir+"/"+dump_name
    dump(dicoToxlsx, path_dump)

    return path_dump


def createDicoOntology(path):
    """_summary_

    Args:
        path (_type_): _description_

    Returns:
        _type_: _description_
    """
    dicoOntology = {}
    ONTOLOGY = open(path, 'r')

    lstLines = ONTOLOGY.read().split("\n")
    ONTOLOGY.close()
    for line in lstLines:
        if line != "" and line[0] != "#":
            splitLine = line.split("\t")
            # print(splitLine)
            if splitLine[-1] not in dicoOntology.keys():
                dicoOntology[splitLine[-1]] = []
            dicoOntology[splitLine[-1]].append(splitLine[0])
    return dicoOntology


def importTranscritList(path):

    with open(path, "r") as fichier:
        panel_list = fichier.readlines()

    transcrit_list = [s.strip() for s in panel_list]

    return transcrit_list


def applyFilters(df_input, config):
    """

    Args:
        df_input (_type_): _description_
        config (_type_): _description_

    Returns:
        _type_: _description_
    """

    # # Create variable filled in config file
    # if iterate_dictionary(config, "project_config/Variables"):

    #     for future_variable, variable_ctique in iterate_dictionary(config,
    #                                                                "project_config/Variables")[0].items():

    #         if variable_ctique["type_input"] == "path":
    #             locals()[future_variable] = globals()[
    #                 variable_ctique["function"]](variable_ctique["valeur"])

    #         if variable_ctique["type_input"] == "list":

    #             locals()[future_variable] = variable_ctique["valeur"]

    # Change type on column if it possible.
    # To apply all filter correctly

    for key in config.keys():

        if key in["transcrit_list","topTwenty","research"]:
             locals()[key] = importTranscritList(config[key])

    df = copy.deepcopy(df_input)
    for col in df.columns:

        if col != "Feature":
            try:
                series = pd.to_numeric(df[col])

                df[col] = series
            except:

                continue

    # Create new columns based on config file
    if config != "" and iterate_dictionary(config, "project_config/Colonnes"):
        confColToCreate = iterate_dictionary(
            config, "project_config/Colonnes")[0]
        for futureColName in confColToCreate.keys():

            if futureColName not in df.columns:
                df_empty = pd.DataFrame([np.nan]*df.shape[0],columns=[futureColName],index=df.index)

                
                df = pd.concat([df,df_empty],axis=1)


                #df[futureColName] = np.nan

            if confColToCreate[futureColName] is None:
                continue

            for futureValue in confColToCreate[futureColName].keys():

                filtre = confColToCreate[futureColName][futureValue]["filtre"].strip(
                )

                try:
                    query = filtre.format(**locals())

                except Exception as error_message:
                    raise Exception(f"""
                    
                        ERROR: Reformat command  
                        
                            `{filtre}` 

                        You specify variable that doesn't exist ({error_message}) 
                        Please if you use variable, place your variable
                        between brackets and put your variable in 
                        in "variableToCreate" category in your config file.
                    """)

                try:

                    listIndex = df.query(query).index.to_list()
                except Exception as e:
                    raise Exception(f"""
                    Error during query with {query}: {e}
                    """)

                if len(listIndex) > 0:
                    df.loc[listIndex, futureColName] = str(futureValue)
    if iterate_dictionary(config, "project_config/keepColumns"):

        colsKeep = iterate_dictionary(config, "project_config/keepColumns")
        realColKeep = []

        for cols in colsKeep:
            try:
                colList = df.loc[:, df.columns.str.contains(f"{cols}",regex=True)].columns.to_list()


                if len(colList)==0:
                    raise Exception(f"""We cannot find your regex columns ({cols}). 
                                    Please check in this list if your target column 
                                    is present: 
                                    
                                    {df.columns.to_list()}""")
                realColKeep.extend(colList)
                    
            except:
                raise Exception(f"""Problem cannot select column {cols}.
                                Your regex is not correct. 
                                Please check in this list if your target column 
                                is present: 
                                    
                                {df.columns.to_list()}""")

        df = df.loc[:,realColKeep]

        
    return df

def getConfigDf(confDict):
    list_df = []
    
    if iterate_dictionary(confDict,"project_config/Variables"):
        tmpDict = {"Variables":{},"Valeurs":{}}
        for index,(future_variable, variable_ctique) in enumerate(iterate_dictionary(confDict,
                                                            "project_config/Variables")[0].items()):
            
            tmpDict["Variables"][index] = future_variable
            tmpDict["Valeurs"][index] = variable_ctique["valeur"]

        list_df.append(pd.DataFrame(tmpDict))
    
    if iterate_dictionary(confDict,"project_config/Colonnes"):      
        tmpDict = {"Colonnes":{},"Valeurs":{},"Filtres":{}}
        iterator = 0
        for index,(colonne, valeur) in enumerate(iterate_dictionary(confDict,
                                                          "project_config/Colonnes")[0].items()):
            if valeur == None:
                
                tmpDict["Colonnes"][iterator] = colonne
                tmpDict["Valeurs"][iterator] = "."
                tmpDict["Filtres"][iterator] = "."
                iterator+=1
                
            if isinstance(valeur,dict):
                
                for value_col,value_ctique in valeur.items():
                    
                    tmpDict["Colonnes"][iterator] = colonne 
                    tmpDict["Valeurs"][iterator] = value_col
                    tmpDict["Filtres"][iterator] = value_ctique["filtre"]
                    iterator+=1
        list_df.append(pd.DataFrame(tmpDict))
        
        if iterate_dictionary(confDict,"project_config/Seuil_Kovergraph"): 
            tmpDict = pd.DataFrame({"Seuil":{0:iterate_dictionary(confDict,
                                                                  "project_config/Seuil_Kovergraph")[0]}})
            list_df.append(tmpDict)

    return list_df


def reformatDf(listpath, confDict, tmp_dir,
               ontology_path, KeyColsForConcat,
               sampleList):
    dictDfReformated = {}

    dictColToRename = {"CLIN_SIG": "CLINVAR",
                       "gnomAD": "GNOMADEX2.1",
                       "MAX_AF": "MAX_AF_GNOMADEX2.1",
                       "CADD_phred": "CADD"}

    dicoToDf = pd.concat([pd.DataFrame(load(path))
                         for path in listpath], axis=0)
    

    dict_df = correctDictValues(dicoToDf)

    # Modification de la structure du tableau pour application des filtres

    for titleCat, df in dict_df.items():

        if titleCat == "CSQ":
            columnsSeries = pd.Series(df.columns)

            df["VARSOME"] = "https://varsome.com/variant/hg38/"+df.CHR + \
                "%3A"+df.POS.astype("str")+"%3A"+df.REF+"%3A"+df.ALT
            df["FeatureWithoutVersion"] = df["Feature"].str.split(".", expand=True)[
                0]
            
            if "EXON" in df.columns and "INTRON" in df.columns:
                df["REGIONS"] = ""

                for index, row in df.iterrows():

                    if row['EXON'] != "" and pd.isna(row['EXON']) == False:

                        value = "E" + "E" + \
                            row["EXON"].split(
                                "/")[0]+" ("+row["EXON"].split("/")[1]+")"
                    elif row['INTRON'] != "" and pd.isna(row['INTRON']) == False:
                        value = "I" + "I" + \
                            row["INTRON"].split(
                                "/")[0]+" ("+row["INTRON"].split("/")[1]+")"
                    else:
                        value = ""

                    df.loc[index, "REGIONS"] = value

            if "Feature" in df.columns:

                df = df.loc[((~df.Feature.astype("str").str.contains("XM")) & (
                    ~df.Feature.astype("str").str.contains("XR"))), :]

            if "HGVSc" in df.columns:
                try:
                    hgvscssplitted = df.HGVSc.str.split(":", expand=True)
                    tabHGVSSplitted = hgvscssplitted[0].astype("str").str.replace(
                        "\.[0-9]*", "", regex=True) + ":" + hgvscssplitted[1]

                    df = df.assign(
                        HGVSc_withoutVersion=tabHGVSSplitted.to_list())
                except KeyError:
                    # df.drop("HGVSc_withoutVersion", axis = 1, inplace = True)
                    df = df.assign(HGVSc_withoutVersion=np.nan)

                df = df.assign(SPLICE_INTRON="")

                
                maskNo = (~df["HGVSc"].astype("str").str.contains('\+', na=False) |
                        ~df["HGVSc"].astype("str").str.contains('\-', na=False))
                maskYes = (df["HGVSc"].astype("str").str.contains('\+', na=False) |
                        df["HGVSc"].astype("str").str.contains('\-', na=False))

                df.loc[maskNo, "SPLICE_INTRON"] = "No"
                df.loc[maskYes, "SPLICE_INTRON"] = "Yes"
                
            if "HGVSp" in df.columns :
                df["HGVSp"] = df.HGVSp.apply(changeHGVSp)

            if "BIOTYPE" in df.columns:
                df["TYPE"] = ""
                df.loc[(df["BIOTYPE"].astype("str").str.contains(
                    "pseudo")), "TYPE"] = "pseudo"
                df.loc[(df["BIOTYPE"] == 'protein_coding'),
                       "TYPE"] = "protein_coding"

            if "SIFT" in df.columns:

                df["SIFT"] = df["SIFT"].str.replace(
                    "deleterious_low_confidence", "D")
                df["SIFT"] = df["SIFT"].str.replace("deleterious", "D+")

                df["SIFT"] = df["SIFT"].str.replace(
                    "tolerated_low_confidence", "B")
                df["SIFT"] = df["SIFT"].str.replace("tolerated", "B+")

            if "PolyPhen" in df.columns:
                df["PolyPhen"] = df["PolyPhen"].str.replace(
                    "probably_damaging", "D+")
                df["PolyPhen"] = df["PolyPhen"].str.replace(
                    "possibly_damaging", "D")
                df["PolyPhen"] = df["PolyPhen"].str.replace("benign", "B")
                df["PolyPhen"] = df["PolyPhen"].str.replace("unknom", "?")

            if (ontology_path != "") and ("Consequence" in df.columns):
                dicoOntology = createDicoOntology(ontology_path)

                df["CSQ"] = ""

                for onto, vep_csq in dicoOntology.items():
                    df.loc[df["Consequence"].isin(vep_csq), "CSQ"] = onto

            if columnsSeries.str.contains("SpliceAI").any():

                df["SpliceAI_num"] = df.loc[:,["SpliceAI_pred_DS_AG","SpliceAI_pred_DS_AL",
                                               "SpliceAI_pred_DS_DG","SpliceAI_pred_DS_DL"]].max(axis=1)

                df["SpliceAI_num"].replace("",np.nan,inplace=True)
                df["SpliceAI_num"] = df['SpliceAI_num'].astype('float64')
                maskHigh = (df["SpliceAI_num"] >= 0.8)
                maskMod = (df["SpliceAI_num"].between(
                    0.5, 0.8, inclusive="right"))
                maskLow = (df["SpliceAI_num"] <= 0.2)
                
                
                
                df["SpliceAI"] = ""

                df.loc[maskHigh, "SpliceAI"] = "High"
                df.loc[maskMod, "SpliceAI"] = "Moderate"
                df.loc[maskLow, "SpliceAI"] = "Low"
                
                # print(df["SpliceAI_num"])
                
        if titleCat == "SPiP":
            df.rename(columns={"Interpretation":"SPiP_Interpretation",
                                "InterConfident":"SPiP_InterConfident"},inplace=True)

        if len(df) == 0:
            return
        df = changeColumnsPatterns(df, dictColToRename)
        
        concatColumns(df,sampleList)

        dictDfReformated[titleCat] = df

    dfConcatenated = False

    if len(dict_df.keys()) > 1:
        for titleCat in KeyColsForConcat.keys():
            if titleCat in dict_df.keys():
                indexForConcat = ["CHR", "POS", "REF", "ALT"]
                
                if isinstance(KeyColsForConcat[titleCat], list):
                    indexForConcat.extend(KeyColsForConcat[titleCat])
                    
                elif isinstance(KeyColsForConcat[titleCat], str):
                    indexForConcat.append(KeyColsForConcat[titleCat])
                    
                dictDfReformated[titleCat] = dictDfReformated[titleCat].assign(
                    FutureIndex=dictDfReformated[titleCat].loc[:, indexForConcat].astype("str").agg('-'.join, axis=1))

                dictDfReformated[titleCat].set_index(
                    "FutureIndex", inplace=True)

                dfConcatenated = True
    # print(dictDfReformated)
    # print(dfConcatenated)
    # raise
    if dfConcatenated:
        # print([*dictDfReformated.values()])
        df = pd.concat([*dictDfReformated.values()], axis=1,
                       verify_integrity=False)

        # print(df)

        df.reset_index(inplace=True, allow_duplicates=True, drop=True)
        
        df = df.loc[:,~df.columns.duplicated()]


    listCol = df.filter(regex="(^RN[A-Z]*_)|(^PJ[A-Z]*_)|(^GNOMAD*|^MAX_AF_GNOMAD*)").columns

    for col in listCol :
        
        df[col].replace(np.nan,0,inplace=True)
        df[col].replace("",0,inplace=True)
    # print(listCol)
    # df.loc[:,["GNOMADEX2.1_AF","MAX_AF_GNOMADEX2.1",
    #           "GNOMADEX2.1_AFR_AF","GNOMADEX2.1_EAS_AF"]].to_csv("exempleCol.csv")
    # raise
        
    colAF = df.loc[:, df.columns.str.contains(f"\nAF$",regex=True)].columns.to_list()
    
    for sampleAF in colAF: 
        
        df[f"{sampleAF}_PCT"] = df[colAF]*100
        

    dictDfReformated = {}


    df = applyFilters(df, confDict)

    df.fillna("", inplace=True)

    dump_name = uuid.uuid4().hex
    pathTmp = tmp_dir+"/"+dump_name
    lenDf = len(df)
    dump(df, pathTmp)

    dictDfReformated[f"Variants-{lenDf}"] = pathTmp

    return dictDfReformated


def changeColumnsPatterns(df, dicoPattern):

    for regex_pat, value_replaced in dicoPattern.items():
        colRegex = df.filter(regex=regex_pat).columns

        for colToChange in colRegex:
            newName = re.sub(regex_pat, value_replaced, colToChange)
            df.rename(columns={colToChange: newName},inplace=True)

    return df

def concatColumns(df,sampleList):
    
    for sample in sampleList:
        listColumnSample = list(filter(lambda x: sample in x, df.columns))
        dictCat={}
        if len(listColumnSample) > 1:
            for column in listColumnSample: 

                try: cat = re.findall("(?:AF$)|(?:CALL[A-Z]*)|(?:DP$)|(?:GT$)|(?:SB$)",column)[0]
                except:
                    break

                
                if cat not in dictCat.keys():
                    dictCat[cat] = {"subcat":[],"columns":[]}
                splitted = column.split("_")

                if isinstance(splitted,str):
                    dictCat[cat]["subcat"].append(splitted)
                    dictCat[cat]["columns"].append(column)
                elif isinstance(splitted,list):
                    dictCat[cat]["subcat"].append(splitted[-1])
                    dictCat[cat]["columns"].append(column)

            for cat in dictCat.keys():
                if len(dictCat[cat]["subcat"]) > 1:
                    df.loc[:,dictCat[cat]["columns"]].fillna(".",inplace=True)
                    df[f'{sample}\n{cat}_{"|".join(dictCat[cat]["subcat"])}'] = df[dictCat[cat]["columns"]].fillna(".").apply(
                        lambda row: '|'.join(row.values.astype(str)), axis=1)

    
    # print(df.columns.to_list())
    # raise      
    return df 

def formatDict(book,listSample,listCategory):
    colors = {0:{"light":"#f2d8c6",
                 "dark":"#e3ae88"},
              1:{"light":"#cbf0bc",
                 "dark":"#9ae27d"},
              2:{"light":"#e7ce92",
                 "dark":"#d9b253"},}
    colors = {0:{"light":"#ECD1F6",
                "dark":"#D9ACEA"},
            1:{"light":"#cbf0bc",
                "dark":"#9ae27d"},
            2:{"light":"#e7ce92",
                "dark":"#d9b253"},}
    colors = {0:{"light":"#F3A2B8",
                "dark":"#F7C2D0"},
            1:{"light":"#cbf0bc",
                "dark":"#9ae27d"},
            2:{"light":"#e7ce92",
                "dark":"#d9b253"},}
    colors = {0:{"light":"#E1F2F7",
                "dark":"#B4CFD7"},
            1:{"light":"#cbf0bc",
                "dark":"#9ae27d"},
            2:{"light":"#e7ce92",
                "dark":"#d9b253"},}
    
    dictFormat = {}
    
    for cat in listCategory:
        dictFormat[cat] = {}
        if cat == "Variants" or cat =="VCF":
    
            dictFormat[cat]["header"] = book.add_format({'bold': 1,'border': 1,'align': 'center',
                                            'valign': 'vcenter','text_wrap':True})

            dictFormat[cat]["dark_row_basic"] = book.add_format({'bold': 0,'border': 1,
                                                            'align': 'center',
                                                            'valign': 'vcenter',
                                                            'bg_color': '#C2C1C2'})
    
            dictFormat[cat]["light_row_basic"] = book.add_format({'bold': 0,'border': 1,
                                                            'align': 'center',
                                                            'valign': 'vcenter'})
    
            dictFormat[cat]["hyperlink"] = book.add_format({'bold': 0,'underline':1,
                                                    'border': 1,
                                                    'align': 'center',
                                                    'valign': 'vcenter'})
    
            for index, sample in enumerate(listSample):
                dictFormat[cat][f"dark_row_{sample}"] = book.add_format({'bold': 0,'border': 1,
                                                                    'align': 'center','valign': 'vcenter',
                                                                    'bg_color': colors[index]["dark"]})
                dictFormat[cat][f"light_row_{sample}"] = book.add_format({'bold': 0,'border': 1,
                                                                'align': 'center','valign': 'vcenter',
                                                                'bg_color': colors[index]["light"]})
        if cat == "Blanket":
            dictFormat[cat]["good_cov"] = book.add_format({'align': 'center', 'valign': 'vcenter',
                                                           'bg_color': '#C6EFCE',
                               'font_color': '#006100','border': 2})
            dictFormat[cat]["bad_cov"] = book.add_format({'align': 'center', 'valign': 'vcenter',
                                                          'bg_color': '#FFC7CE',
                               'font_color': '#9C0006','border': 2})
            dictFormat[cat]["merge_cols"] = book.add_format({'align': 'center',
                                                              'valign': 'vcenter', 
                                                              'border': 2})
            
            
    

    
    return dictFormat

def sheetDict(book,listcat):
    dictWorkSheet = {}
    for cat in listcat:
        dictWorkSheet[cat] = {}
        dictWorkSheet[cat]["sheet"] = book.add_worksheet(cat)
        dictWorkSheet[cat]["numberCols"] = 0
        dictWorkSheet[cat]["numberRows"] = 0
        
    
    return dictWorkSheet


def writeVariant(dictFormat,dictsheet,tmp_path,writeHeader,
                listSample,darkFormat,config):
    """
    Write all lines cotnaines in pandas dataframe 
    imported with tmp_path to write a xlsx file.
    Return shape of df treated to adapt coordinates
    with xlsxwriter module. 

    Args:
        dictFormat (dict): Dictionnary with all cells format
                           (Output formatDict function)
        dictsheet  (dict): Dictionnary with all xlsxwriter sheet. 
                           All sheets are named by their keys 
        tmp_path (str): Path of pandas dataframe with variants
        writeHeader (boolean): Boolean to determinate if writeVariant need 
                               to take into account the df header in writing
                               xlsx file.
        listSample (list): Sample list contained in vcf columns
        darkFormat (boolean): Boolean to take into account or not dark format 
                            at each change of position  
        config (dict): Configuration specify in yml file.

    Returns:
        tuple: (df.pandas.shape: shape of df treated,
        Boolean: specify if darkFormat or not)
    """

    #Get xlsxwriter.worksheet object
    sheet = dictsheet["sheet"]

    #For Variant category is always an str variable
    #But for other catgory like Blanket or config it can 
    #be an df directly
    if isinstance(tmp_path,str):
        df = load(tmp_path)
        if isinstance(df,dict):
            df= pd.DataFrame(df)
        
    else:
        df= copy.deepcopy(tmp_path)
    
    #If first iteration write header
    if writeHeader:

        listColumn = df.columns.to_list()


        for index_title,title in enumerate(listColumn):

            try: 
                sheet.write_string(0, index_title, title,
                               dictFormat["header"])
            except:
                sheet.write_string(0, index_title, title)

        #if freeze_rows in yml config file
        #sheet.freeze_panes is used.
        if iterate_dictionary(config,"project_config/freeze_rows"):
            sheet.freeze_panes(config["project_config"]["freeze_rows"],
            config["project_config"]["freeze_cols"]) 

    

    #dictsheet["numberRows"] keeps the number of lines already filled
    # in the xlsx file. Define listIndex to write all informations
    listIndex = list(range(dictsheet["numberRows"]+1,
                           dictsheet["numberRows"]+len(df)+1))
    posList = -1  
    for index_row,(index_str, row) in enumerate(df.iterrows()):

        #check if position change between iteration 
        # to activate or deasactivate darkFormat
        if row["POS"] != posList:
            posList = copy.deepcopy(row["POS"])
            if darkFormat:
                darkFormat = False
            else:
                darkFormat = True


        for index, column in enumerate(row.keys()):
            
            patentColumn=False
            
            for sample in listSample:
        
                checkSampleCol = bool(re.match(sample,column))
                
                if checkSampleCol:
                    patentColumn = True
                
            if darkFormat: 
                #Check if column contain sample name
                if patentColumn:
                    formatCase = dictFormat[f"dark_row_{sample}"]
                else: 
                    formatCase = dictFormat["dark_row_basic"]
            else:
                if patentColumn:
                    formatCase = dictFormat[f"light_row_{sample}"]
                else:
                    formatCase = dictFormat["light_row_basic"]
            
            try:
                if isinstance(row[column],list):
                    if len(row[column]) > 0:
                        tmplist = map(str,row[column])
                        row[column] = "-".join(tmplist)
                sheet.write(listIndex[index_row],index,row[column],formatCase)
            except:
                print(index_row)
                print(listIndex)
                print(index)
                print(f"{dictsheet['numberRows']}///{len(df)}")
                print(df)
                raise    
    return df.shape,darkFormat



def corrBlanket(df):

    df.fillna("OTHER",inplace=True)

    for index,  row in df.iterrows():

        if row["Gene"] == "OTHER":

            df.loc[index,"Gene"] = gene
        
        else:
            gene = copy.deepcopy(row["Gene"])
        
        if row["Transcrit"] =="OTHER":


            df.loc[index,"Transcrit"] = transcrit
        
        else:
            transcrit = copy.deepcopy(row["Transcrit"])
    return df
 

def writeBlanket(list_df,format_sheet, sheet,config):
    
    df_brut = list_df[0]
    
    df= corrBlanket(df_brut)
    
    if iterate_dictionary(config, "project_config/Seuil_Kovergraph"):
        
        seuil = iterate_dictionary(config, "project_config/Seuil_Kovergraph")[0]
    else:
        seuil = 30
        
    dictIndex = {"gene":{},"transcrit":{}}

    iterator_row = 0


    for index_col, col in enumerate(df.columns.to_list()):
                    
        sheet.write(iterator_row,index_col,col)
        
    for index,row in df.iterrows():
        gene = row["Gene"]
        transcrit = row["Transcrit"]
        if gene not in dictIndex["gene"].keys():
            dictIndex["gene"][gene] = {}
            dictIndex["gene"][gene]["begin"] = iterator_row + 1
            dictIndex["gene"][gene]["end"] = 0
        
        if transcrit not in dictIndex["transcrit"].keys():
            dictIndex["transcrit"][transcrit] = {}
            dictIndex["transcrit"][transcrit]["begin"] = iterator_row + 1
            dictIndex["transcrit"][transcrit]["end"] = 0
        
        iterator_row +=1
        dictIndex["gene"][gene]["end"] = iterator_row
        dictIndex["transcrit"][transcrit]["end"] = iterator_row
    
    
        for index_value, value in enumerate(row):
    
            if index_value > 1:
                sheet.write(iterator_row,index_value,value)
        
    for name in dictIndex.keys():
        
        if name == "gene":
        
            for gene in dictIndex["gene"].keys():
            
                sheet.merge_range(dictIndex["gene"][gene]["begin"],0,
                              dictIndex["gene"][gene]["end"],0,gene,format_sheet["merge_cols"])
        else:
            
            for transcrit in dictIndex["transcrit"].keys():
            
             sheet.merge_range(dictIndex["transcrit"][transcrit]["begin"],1,
                              dictIndex["transcrit"][transcrit]["end"],1,transcrit,format_sheet["merge_cols"])    
            
    sheet.conditional_format(1,3,
                            df.shape[0],
                            df.shape[1]-1,{'type':     'cell',
                                        'criteria': '<',
                                        'value':    seuil,
                                        'format':   format_sheet["bad_cov"]}
                            )   
    sheet.conditional_format(1,3,
                            df.shape[0],
                            df.shape[1]-1,
                            {'type':     'cell',
                            'criteria': '>=',
                            'value':    seuil,
                            'format':   format_sheet["good_cov"]}
                            )  
    
    return
        
            
def writeExcel(dictListDf,output_name,
               sampleList,confDict):
    """
    Write excel file with all variants
    contained in vcf file. 

    Args:
        dictListDf (dict): Dictionary with all variants, and other data that 
                             will be filled in sheets.
        output_name (str): name of output file.
        sampleList (list): list of sample name contained in vcf file.
        confDict (dict): dict with all configurations.
    """
                                      
    with xlsxwriter.Workbook(output_name,{'constant_memory': False,
                                          'strings_to_numbers': True,
                                          'strings_to_urls': True,
                                          "nan_inf_to_errors": True
                                          }) as workbook:
        
        dictFormat = formatDict(workbook,sampleList,dictListDf.keys())
        
        dictSheet = sheetDict(workbook,dictListDf.keys())
        
        darkFormat = False
        
        for cat,list_df in dictListDf.items():

            if cat == "Blanket":

                writeBlanket(list_df,dictFormat[cat],
                             dictSheet[cat]["sheet"],
                             confDict)
                
                dictSheet[cat]["numberCols"] = list_df[0].shape[1]
                dictSheet[cat]["numberRows"] = list_df[0].shape[0]
                
                
            if cat == "Config":
                print("")
                # writeConfig(dictFormat[cat],dictSheet[cat]["sheet"],confDict)
                
                
            if cat == "Variants" or cat =="VCF":

                for index_path,path in enumerate(list_df):

                    if index_path == 0:
                
                        (lenRowTreated,lenColumn),darkFormat = writeVariant(dictFormat[cat],
                                                                       dictSheet[cat],
                                                                       path, True,
                                                                       sampleList,
                                                                       darkFormat,
                                                                       confDict)
                        dictSheet[cat]["numberCols"] = lenColumn
                        dictSheet[cat]["numberRows"] += lenRowTreated
                    
                    else:
                        (lenRowTreated,lenColumn),darkFormat = writeVariant(dictFormat[cat],
                                                                        dictSheet[cat],
                                                                        path, False,
                                                                        sampleList,
                                                                        darkFormat,
                                                                        confDict)
                        dictSheet[cat]["numberRows"] += lenRowTreated

                    
        for cat in dictSheet.keys():
            
            if cat != "Config":

                dictSheet[cat]["sheet"].autofit()

                
                try:

                    dictSheet[cat]["sheet"].autofilter(0,
                                                0,
                                                dictSheet[cat]["numberRows"],
                                                dictSheet[cat]["numberCols"]-1)

                except:
                    continue


    
    